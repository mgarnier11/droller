import { Injectable } from "@angular/core";

// If you import a module but never use any of the imported values other than as TypeScript types,
// the resulting javascript file will look as if you never imported the module at all.

import { ElectronService } from "./electron.service";
import { TelloState } from "../models/telloState";

@Injectable({
  providedIn: "root",
})
export class DroneStateService {
  private _ticker: NodeJS.Timeout;

  public droneState: TelloState = undefined;

  constructor(private _electronService: ElectronService) {
    this._tick = this._tick.bind(this);

    this._ticker = setInterval(this._tick, 100);
  }

  private async _tick() {
    this.droneState = await this._electronService.ipcRenderer.invoke(
      "getDroneState"
    );
  }
}

import { app, BrowserWindow, ipcMain, screen } from "electron";
import * as path from "path";
import * as url from "url";
import { TelloDrone } from "./electronSrc/tello/telloDrone.class";
import { FlipDirections } from "./electronSrc/tello/telloSdk.class";
import { TelloStream } from "./electronSrc/tello/telloStream.class";

let win: BrowserWindow = null;
const args = process.argv.slice(1),
  serve = args.some((val) => val === "--serve");

const telloDrone = new TelloDrone("192.168.10.1");
const telloStream = new TelloStream();

function createWindow(): BrowserWindow {
  const electronScreen = screen;
  const size = electronScreen.getPrimaryDisplay().workAreaSize;

  // Create the browser window.
  win = new BrowserWindow({
    x: 0,
    y: 0,
    width: size.width / 2,
    height: size.height / 2,
    webPreferences: {
      nodeIntegration: true,
      allowRunningInsecureContent: serve ? true : false,
      contextIsolation: false, // false if you want to run 2e2 test with Spectron
      enableRemoteModule: true, // true if you want to run 2e2 test  with Spectron or use remote module in renderer context (ie. Angular)
    },
  });

  if (serve) {
    win.webContents.openDevTools();

    require("electron-reload")(__dirname, {
      electron: require(`${__dirname}/node_modules/electron`),
    });
    win.loadURL("http://localhost:4200");
  } else {
    win.loadURL(
      url.format({
        pathname: path.join(__dirname, "dist/index.html"),
        protocol: "file:",
        slashes: true,
      })
    );
  }

  // Emitted when the window is closed.
  win.on("closed", () => {
    // Dereference the window object, usually you would store window
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null;
  });

  return win;
}

try {
  // This method will be called when Electron has finished
  // initialization and is ready to create browser windows.
  // Some APIs can only be used after this event occurs.
  // Added 400 ms to fix the black background issue while using transparent window. More detais at https://github.com/electron/electron/issues/15947
  app.on("ready", () => setTimeout(createWindow, 400));

  // Quit when all windows are closed.
  app.on("window-all-closed", () => {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== "darwin") {
      app.quit();
    }
  });

  app.on("activate", () => {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (win === null) {
      createWindow();
    }
  });

  ipcMain.handle("getDroneState", async (e) => {
    return {
      ...telloDrone.state,
      connected: telloDrone.connected,
      isFlying: telloDrone.isFlying,
      streaming: telloDrone.streaming,
      speed: telloDrone.speed,
      streamServerStarted: telloStream.started,
    };
  });

  ipcMain.on("startTelloStream", (e) => {
    telloStream.startTelloStreamServer(3000).then((succes) => {
      console.log("stream server started : ", succes);
      telloDrone.startStream().then(() => {
        console.log("stream shoudl be started");
      });
    });
  });

  ipcMain.on("stopTelloStream", (e) => {
    telloStream.stopTelloStreamServer().then((succes) => {
      console.log("stream server stopped : ", succes);
      telloDrone.stopStream().then(() => {
        console.log("stream shoudl be stopped");
      });
    });
  });

  ipcMain.on("gamepadThumbsticks", (e, datas: number[]) => {
    telloDrone.gamepadControl(datas);
  });
  ipcMain.on("gamepadButtonDown", (e, datas: string) => {
    console.log("button down : ", datas);
  });

  ipcMain.on("gamepadButtonUp", (e, datas: string) => {
    console.log("button up : ", datas);
    switch (datas) {
      case "BUTTON_MENU":
        console.log("should be connecting to drone");

        telloDrone.connectToDrone();
        break;
      case "BUTTON_VIEW":
        if (!telloStream.started) {
          console.log("should be starting stream server");

          telloStream.startTelloStreamServer(3000);
        }

        if (telloDrone.streaming) {
          console.log("should be stoping stream");

          telloDrone.stopStream();
        } else {
          console.log("should be starting stream");

          telloDrone.startStream();
        }

        break;
      case "X":
        if (telloDrone.isFlying) {
          console.log("should be landing to drone");

          telloDrone.land();
        } else {
          console.log("should be taking drone off");

          telloDrone.takeOff();
        }
        break;
      case "D_PAD_UP":
        telloDrone.flip(FlipDirections.Forward);
        break;
      case "D_PAD_DOWN":
        telloDrone.flip(FlipDirections.Backward);
        break;
      case "D_PAD_LEFT":
        telloDrone.flip(FlipDirections.Left);
        break;
      case "D_PAD_RIGHT":
        telloDrone.flip(FlipDirections.Right);
        break;
    }
  });

  ipcMain.on("gamepadButtonPressed", (e, datas: string) => {
    switch (datas) {
      case "TRIGGER_RIGHT":
        telloDrone.speedUp();
        break;
      case "TRIGGER_LEFT":
        telloDrone.speedDown();
        break;
    }
  });
} catch (e) {
  // Catch Error
  // throw e;
}

import * as JSMpeg from "@cycjimmy/jsmpeg-player";

import { AfterViewInit, Component, OnInit, ViewChild } from "@angular/core";
import { ElectronService } from "../../services/electron.service";
import { GamepadService } from "../../services/gamepad.service";
import { DroneStateService } from "../../services/droneState.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
})
export class HomeComponent implements OnInit, AfterViewInit {
  private _videoElement;
  @ViewChild("videoWrapper") videoWrapper;

  constructor(
    private _electronService: ElectronService,
    public gamepadService: GamepadService,
    public droneStateService: DroneStateService
  ) {}

  ngOnInit(): void {}

  ngAfterViewInit() {
    this._videoElement = new JSMpeg.VideoElement(
      this.videoWrapper.nativeElement,
      "ws://localhost:3000/stream",
      {
        autoplay: true,
      }
    );
  }

  startStream() {
    this._electronService.sendToMain("startTelloStream");
  }

  stopStream() {
    this._electronService.sendToMain("stopTelloStream");
  }
}

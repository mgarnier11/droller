import { Component } from "@angular/core";
import { ElectronService } from "./services/electron.service";
import { AppConfig } from "../environments/environment";
import { GamepadService } from "./services/gamepad.service";
import { DroneStateService } from "./services/droneState.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent {
  constructor(
    private _electronService: ElectronService,
    private _gamepadService: GamepadService,
    private _droneStateService: DroneStateService
  ) {
    console.log("AppConfig", AppConfig);

    if (this._electronService.isElectron) {
      console.log(process.env);
      console.log("Run in electron");
      console.log("Electron ipcRenderer", this._electronService.ipcRenderer);
      console.log("NodeJS childProcess", this._electronService.childProcess);
    } else {
      console.log("Run in browser");
    }
  }
}

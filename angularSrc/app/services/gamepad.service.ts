import { Injectable } from "@angular/core";
import { ElectronService } from "./electron.service";
import * as buttonsJson from "../xbox.json";

const buttons = (buttonsJson as any).default;

@Injectable({
  providedIn: "root",
})
export class GamepadService {
  private _ticker: NodeJS.Timeout;
  private _gamepadId: string;

  private _lastPressedButtons: GamepadButton[];

  public get gamepadConnected() {
    return this._gamepadId !== undefined;
  }

  constructor(private _electronService: ElectronService) {
    console.log("buttons", buttons);

    this._tick = this._tick.bind(this);

    window.addEventListener("gamepadconnected", (e: GamepadEvent) => {
      console.log("gamepad connected");
      console.log(e);

      if (this._gamepadId === undefined) {
        this._gamepadId = e.gamepad.id;
      }
    });

    window.addEventListener("gamepaddisconnected", (e: GamepadEvent) => {
      console.log("gamepad disconnected");

      if (e.gamepad.id === this._gamepadId) {
        this._gamepadId = undefined;
        this._lastPressedButtons = undefined;
      }
    });

    this._ticker = setInterval(this._tick, 25);
  }

  private _getGamepad() {
    const gamepads = Array.from(navigator.getGamepads());

    return gamepads.find((gp) => gp?.id === this._gamepadId);
  }

  private _tick() {
    const gamepad = this._getGamepad();

    // console.log(gamepad);

    if (gamepad) {
      const axes = gamepad.axes.map((axe) => (Math.abs(axe) > 0.05 ? axe : 0));

      this._electronService.sendToMain("gamepadThumbsticks", axes);

      for (let i = 0; i < gamepad.buttons.length; i++) {
        const jsonButton = buttons[i];

        const button = gamepad.buttons[i];

        if (this._lastPressedButtons) {
          const lastButtonState = this._lastPressedButtons[i];

          if (!lastButtonState.pressed && button.pressed) {
            this._electronService.sendToMain("gamepadButtonDown", jsonButton);
          } else if (lastButtonState.pressed && !button.pressed) {
            this._electronService.sendToMain("gamepadButtonUp", jsonButton);
          }

          if (button.pressed == true) {
            this._electronService.sendToMain(
              "gamepadButtonPressed",
              jsonButton
            );
          }
        }
      }

      this._lastPressedButtons = gamepad.buttons.map((btn) => btn);
    }
  }
}

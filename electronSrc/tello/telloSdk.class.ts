import * as dgram from "dgram";
import { EventEmitter } from "events";
import { REJECT_TIMEOUT, promiseTimeout } from "../utils";

enum TelloControlCommands {
  Command = "command",
  TakeOff = "takeoff",
  Land = "land",
  StreamOn = "streamon",
  StreamOff = "streamoff",
  Emergency = "emergency",
  Up = "up",
  Down = "down",
  Left = "left",
  Right = "right",
  Forward = "forward",
  Backward = "back",
  ClockWise = "cw",
  CounterClockWise = "ccw",
  Flip = "flip",
  Go = "go",
  // Stop = 'stop',
  Curve = "curve",
  Speed = "speed",
  RC = "rc",
  Wifi = "wifi",
}

export enum FlipDirections {
  Left = "l",
  Right = "r",
  Forward = "f",
  Backward = "b",
}

export class TelloSdk {
  private _telloIp: string = "";
  private _telloPort: number;
  private _telloStatePort: number;
  private _showLogs: boolean = false;

  private __initialized: boolean = false;
  private get _initialized(): boolean {
    return this.__initialized;
  }
  private set _initialized(value: boolean) {
    this.__initialized = value;
    this.events.emit("initialized", this.__initialized);
  }

  public events: EventEmitter = new EventEmitter();

  private _droneUdpClient: dgram.Socket;
  private _droneStateUdpClient: dgram.Socket;

  /**
   *
   */
  constructor(telloPort: number, telloStatePort: number, showLogs = false) {
    this._telloPort = telloPort;
    this._telloStatePort = telloStatePort;
    this._showLogs = showLogs;

    this._droneUdpClient = dgram.createSocket("udp4");
    this._droneUdpClient.bind(this._telloPort);

    this._droneUdpClient.setMaxListeners(50);

    this._droneStateUdpClient = dgram.createSocket("udp4");
    this._droneStateUdpClient.bind(this._telloStatePort);

    this._droneStateUdpClient.setMaxListeners(50);

    this._droneStateUdpClient.on("message", (msg, rinfo) => {
      const stringMsg = msg.toString();

      this.events.emit("droneStateMessage", stringMsg);
      this._log("drone state message : ", stringMsg);
    });

    this._droneUdpClient.on("message", (msg, rinfo) => {
      const stringMsg = msg.toString();

      this.events.emit("droneMessage", stringMsg);
      this._log("drone message : ", stringMsg);
    });
  }

  private _log(message?: any, optParams?: any) {
    if (this._showLogs) {
      console.log(message, optParams);
    }
  }

  public connect(telloIp: string, retryOnTimeOut = false) {
    this._telloIp = telloIp;

    this._log(`Connecting to ${telloIp}`);

    const tryConnect = (retryOnTimeOut: boolean) => {
      return new Promise<boolean>((res, rej) => {
        promiseTimeout(
          5000,
          new Promise<boolean>((res, rej) => {
            const messageReceived = (msg: Buffer, rInfos: dgram.RemoteInfo) => {
              const stringMsg = msg.toString();

              if (stringMsg == "ok") {
                res(true);
              } else {
                res(false);
              }
            };

            this._droneUdpClient.once("message", messageReceived);

            this._droneUdpClient.send(
              TelloControlCommands.Command,
              this._telloPort,
              this._telloIp,
              (error, bytesSend) => {
                if (error) rej(error);
                this._log(`${bytesSend} bytes sent`);
              }
            );
          })
        )
          .then((droneConnected: boolean) => {
            res(droneConnected);
          })
          .catch(async (error) => {
            if (error === REJECT_TIMEOUT) {
              if (retryOnTimeOut) {
                this._log("Timed out, retrying drone connection");

                this._droneUdpClient.removeAllListeners("message");

                res(await tryConnect(retryOnTimeOut));
              } else {
                res(false);
              }
            } else {
              res(false);
            }
          });
      });
    };

    return tryConnect(retryOnTimeOut);
  }

  private _sendControlCommand(
    command: TelloControlCommands,
    ...args: string[]
  ) {
    const argString = args.join(" ");
    const commandToSend =
      command + (argString.length > 0 ? ` ${argString}` : "");

    let onceMessage: any;

    return new Promise<boolean>((res) => {
      promiseTimeout<boolean>(
        100,
        new Promise<boolean>((resolve, reject) => {
          onceMessage = (msg, rinfos) => {
            const stringMsg = msg.toString();

            resolve(stringMsg === "ok");
          };

          this._droneUdpClient.once("message", onceMessage);

          this._droneUdpClient.send(
            commandToSend,
            this._telloPort,
            this._telloIp,
            (error, bytesSend) => {
              if (error) reject(error);
              this._log(`${bytesSend} bytes sent`);
            }
          );
        })
      )
        .then((commandSucces) => res(commandSucces))
        .catch((err) => {
          if (err !== REJECT_TIMEOUT) {
            console.log(err);
          }

          this._droneUdpClient.removeListener("message", onceMessage);

          res(false);
        });
    });
  }

  public waitInit() {
    return new Promise<boolean>((res) => {
      if (this._initialized) res(true);
      else {
        this.events.once("initialized", res);
      }
    });
  }

  public isConnected() {
    return this._sendControlCommand(TelloControlCommands.Command);
  }

  /**
   * Send the 'takeoff' command to the connected Tello drone
   *
   * Auto takeoff
   *
   * @returns {Promise<boolean>} Promise object with the result of the command (success or not)
   * @memberof TelloSdk
   */
  public takeOff() {
    return this._sendControlCommand(TelloControlCommands.TakeOff);
  }

  /**
   * Send the 'land' command to the connected Tello drone
   *
   * Auto landing
   *
   * @returns {Promise<boolean>} Promise object with the result of the command (success or not)
   * @memberof TelloSdk
   */
  public land() {
    return this._sendControlCommand(TelloControlCommands.Land);
  }

  /**
   * Send the 'streamon' command to the connected Tello drone
   *
   * Enable video stream
   *
   * @returns {Promise<boolean>} Promise object with the result of the command (success or not)
   * @memberof TelloSdk
   */
  public streamOn() {
    return this._sendControlCommand(TelloControlCommands.StreamOn);
  }

  /**
   * Send the 'streamoff' command to the connected Tello drone
   *
   * Disable video stream
   *
   * @returns {Promise<boolean>} Promise object with the result of the command (success or not)
   * @memberof TelloSdk
   */
  public streamOff() {
    return this._sendControlCommand(TelloControlCommands.StreamOff);
  }

  /**
   * Send the 'emergency' command to the connected Tello drone
   *
   * Stop motors immediately
   *
   * @returns {Promise<boolean>} Promise object with the result of the command (success or not)
   * @memberof TelloSdk
   */
  public emergency() {
    return this._sendControlCommand(TelloControlCommands.Emergency);
  }

  /**
   * Send the 'up' command to the connected Tello drone
   *
   * Fly up for "cm" cm
   *
   * @param {number} cm (between 20 and 500)
   *
   * @returns {Promise<boolean>} Promise object with the result of the command (success or not)
   * @memberof TelloSdk
   */
  public up(cm: number) {
    return this._sendControlCommand(TelloControlCommands.Up, cm.toString());
  }

  /**
   * Send the 'down' command to the connected Tello drone
   *
   * Fly down for "cm" cm
   *
   * @param {number} cm (between 20 and 500)
   *
   * @returns {Promise<boolean>} Promise object with the result of the command (success or not)
   * @memberof TelloSdk
   */
  public down(cm: number) {
    return this._sendControlCommand(TelloControlCommands.Down, cm.toString());
  }

  /**
   * Send the 'left' command to the connected Tello drone
   *
   * Fly left for "cm" cm
   *
   * @param {number} cm (between 20 and 500)
   *
   * @returns {Promise<boolean>} Promise object with the result of the command (success or not)
   * @memberof TelloSdk
   */
  public left(cm: number) {
    return this._sendControlCommand(TelloControlCommands.Left, cm.toString());
  }

  /**
   * Send the 'right' command to the connected Tello drone
   *
   * Fly right for "cm" cm
   *
   * @param {number} cm (between 20 and 500)
   *
   * @returns {Promise<boolean>} Promise object with the result of the command (success or not)
   * @memberof TelloSdk
   */
  public right(cm: number) {
    return this._sendControlCommand(TelloControlCommands.Right, cm.toString());
  }

  /**
   * Send the 'forward' command to the connected Tello drone
   *
   * Fly forward for "cm" cm
   *
   * @param {number} cm (between 20 and 500)
   *
   * @returns {Promise<boolean>} Promise object with the result of the command (success or not)
   * @memberof TelloSdk
   */
  public forward(cm: number) {
    return this._sendControlCommand(
      TelloControlCommands.Forward,
      cm.toString()
    );
  }

  /**
   * Send the 'back' command to the connected Tello drone
   *
   * Fly backward for "cm" cm
   *
   * @param {number} cm (between 20 and 500)
   *
   * @returns {Promise<boolean>} Promise object with the result of the command (success or not)
   * @memberof TelloSdk
   */
  public backward(cm: number) {
    return this._sendControlCommand(
      TelloControlCommands.Backward,
      cm.toString()
    );
  }

  /**
   * Send the 'cw' command to the connected Tello drone
   *
   * Rotate the drone "degrees" clockwise
   *
   * @param {number} degrees (between 1 and 360)
   *
   * @returns {Promise<boolean>} Promise object with the result of the command (success or not)
   * @memberof TelloSdk
   */
  public rotateClockWise(degrees: number) {
    return this._sendControlCommand(
      TelloControlCommands.ClockWise,
      degrees.toString()
    );
  }

  /**
   * Send the 'ccw' command to the connected Tello drone
   *
   * Rotate the drone "degrees" counterclockwise
   *
   * @param {number} degrees (between 1 and 360)
   *
   * @returns {Promise<boolean>} Promise object with the result of the command (success or not)
   * @memberof TelloSdk
   */
  public rotateCounterClockWise(degrees: number) {
    return this._sendControlCommand(TelloControlCommands.CounterClockWise, degrees.toString()); // prettier-ignore
  }

  /**
   * Send the 'flip' command to the connected Tello drone
   *
   * Flip the drone in the chosed direction
   *
   * @param {FlipDirections} direction (direction to flip)
   *
   * @returns {Promise<boolean>} Promise object with the result of the command (success or not)
   * @memberof TelloSdk
   */
  public flip(direction: FlipDirections) {
    return this._sendControlCommand(TelloControlCommands.Flip, direction); // prettier-ignore
  }

  /**
   * Send the 'fly' command to the connected Tello drone
   *
   * Fly to "x" "y" "z" at "speed" (cm/s)
   *
   * @param {number} x (between -500 and 500)
   * @param {number} y (between -500 and 500)
   * @param {number} z (between -500 and 500)
   * @param {number} speed (between 10 and 100)
   *
   * Note "x", "y" and "z" values cant be set between -20 and 20
   *
   * @returns {Promise<boolean>} Promise object with the result of the command (success or not)
   * @memberof TelloSdk
   */
  public go(x: number, y: number, z: number, speed: number) {
    return this._sendControlCommand(
      TelloControlCommands.Go,
      x.toString(),
      y.toString(),
      z.toString(),
      speed.toString()
    );
  }

  // /**
  //  * Send the 'stop' command to the connected Tello drone
  //  *
  //  * Make the drone hovers in the air
  //  *
  //  * @returns {Promise<boolean>} Promise object with the result of the command (success or not)
  //  * @memberof TelloSdk
  //  */
  // stop() {
  //   return this._sendControlCommand(TelloControlCommands.Stop);
  // }

  /**
   * Send the 'curve' command to the connected Tello drone
   *
   * Fly a curve defined by the current and the 2 given points at "speed" (cm/s)
   *
   * @param {number} x1 (between -500 and 500)
   * @param {number} y1 (between -500 and 500)
   * @param {number} z1 (between -500 and 500)
   * @param {number} x2 (between -500 and 500)
   * @param {number} y2 (between -500 and 500)
   * @param {number} z2 (between -500 and 500)
   * @param {number} speed (between 10 and 60)
   *
   * Note "x1", "y1" and "z1" values cant be set between -20 and 20
   * Note "x2", "y2" and "z2" values cant be set between -20 and 20
   *
   * @returns {Promise<boolean>} Promise object with the result of the command (success or not)
   * @memberof TelloSdk
   */
  public curve(
    x1: number,
    y1: number,
    z1: number,
    x2: number,
    y2: number,
    z2: number,
    speed: number
  ) {
    return this._sendControlCommand(
      TelloControlCommands.Curve,
      x1.toString(),
      y1.toString(),
      z1.toString(),
      x2.toString(),
      y2.toString(),
      z2.toString(),
      speed.toString()
    );
  }

  public speed(speed: number) {
    return this._sendControlCommand(
      TelloControlCommands.Speed,
      speed.toString()
    );
  }

  public rc(lr: number, fb: number, ud: number, yaw: number, nbTries = 1) {
    const commandToSend = `${TelloControlCommands.RC} ${lr} ${fb} ${ud} ${yaw}`;

    this._log(`sending ${commandToSend}, ${nbTries} times`);

    for (let i = 0; i < nbTries; i++) {
      this._droneUdpClient.send(
        commandToSend,
        this._telloPort,
        this._telloIp,
        (error, bytesSend) => {
          if (error) this._log(error);

          this._log(`${bytesSend} bytes sent`);
        }
      );
    }
  }
}

import * as http from "http";
import * as ws from "ws";
import { ChildProcessWithoutNullStreams, spawn } from "child_process";

export class TelloStream {
  private _ffmpeg: ChildProcessWithoutNullStreams;
  private _httpServer: http.Server;
  private _wsServer: ws.Server;

  public get started(): boolean {
    return (
      this._ffmpeg !== undefined &&
      this._httpServer !== undefined &&
      this._wsServer !== undefined
    );
  }

  /**
   *
   */
  constructor() {}

  public startTelloStreamServer(port: number) {
    return new Promise<boolean>((resolve, reject) => {
      if (!this._httpServer) {
        this._httpServer = http
          .createServer((req, res) => {
            req.on("data", (data) => {
              // Now that we have data let's pass it to the web socket server
              // telloWebSocketServer.broadcast(data);
              this._wsServer.clients.forEach(function each(client) {
                if (client.readyState === ws.OPEN) {
                  client.send(data);
                }
              });
            });
          })
          .on("listening", () => {
            resolve(true);
          })
          .listen(port);

        this._wsServer = new ws.Server({ server: this._httpServer });

        this._startFFMpeg(port);
      } else {
        resolve(false);
      }
    });
  }

  private _startFFMpeg(port: number, pipeLogs = false) {
    var args = [
      "-i",
      "udp://0.0.0.0:11111",
      "-r",
      "30",
      "-s",
      "960x720",
      "-codec:v",
      "mpeg1video",
      "-b",
      "800k",
      "-f",
      "mpegts",
      `http://127.0.0.1:${port}/stream`,
    ];

    this._ffmpeg = spawn("ffmpeg", args);
    // Uncomment if you want to see ffmpeg stream info
    if (pipeLogs) this._ffmpeg.stderr.pipe(process.stderr);
    this._ffmpeg.on("exit", (code) => {
      console.log("Failure", code);
    });
  }

  public stopTelloStreamServer() {
    return new Promise<boolean>((res) => {
      this._stopFFMpeg();

      this._wsServer.close((err) => {
        console.error(err);

        this._httpServer.close((err) => {
          console.error(err);

          res(true);
        });
      });
    });
  }

  private _stopFFMpeg() {
    this._ffmpeg.kill();
  }
}

import { FlipDirections, TelloSdk } from "./telloSdk.class";

export interface TelloState {
  pitch: number;
  roll: number;
  yaw: number;
  vgx: number;
  vgy: number;
  vgz: number;
  templ: number;
  temph: number;
  tof: number;
  h: number;
  bat: number;
  baro: number;
  time: number;
  agx: number;
  agy: number;
  agz: number;
}

export class TelloDrone {
  private _sdk: TelloSdk;

  private _droneIp: string;

  private _state: TelloState = {
    pitch: 0,
    roll: 0,
    yaw: 0,
    vgx: 0,
    vgy: 0,
    vgz: 0,
    templ: 0,
    temph: 0,
    tof: 0,
    h: 0,
    bat: 0,
    baro: 0,
    time: 0,
    agx: 0,
    agy: 0,
    agz: 0,
  };
  public get state() {
    return this._state;
  }

  private _connected: boolean = false;
  public get connected(): boolean {
    return this._connected;
  }

  public get isFlying(): boolean {
    return this._state.h !== 0;
  }

  // private _lastSpeedChange: number = 0;
  private _speed: number = 10;
  public get speed(): number {
    return this._speed;
  }

  private _streaming: boolean = false;
  public get streaming(): boolean {
    return this._streaming;
  }

  private _lastGeneralStateUpdate = 0;

  private _gamepadState: number[] = [];

  private _ticker: NodeJS.Timeout;

  // private _autoCorrect = true;

  /**
   *
   */
  constructor(droneIp: string) {
    this.stateMessageReceived = this.stateMessageReceived.bind(this);
    this._tick = this._tick.bind(this);

    this._sdk = new TelloSdk(
      TelloDrone.TELLO_PORT,
      TelloDrone.TELLO_STATE_PORT
      // true
    );
    this._droneIp = droneIp;

    this._sdk.events.on("droneStateMessage", this.stateMessageReceived);

    this._ticker = setInterval(this._tick, 50);
  }

  public stateMessageReceived(stringMsg: string) {
    this._processStateMessage(stringMsg);
  }

  private _processStateMessage(stateMsg: string) {
    const dataStrings = stateMsg.split(";");

    for (const dataStr of dataStrings) {
      const datas = dataStr.split(":");

      if (datas.length === 2) {
        const label = datas[0];
        const value = datas[1];

        this._state[label] = parseFloat(value);
      }
    }
  }

  private _tick() {
    this._updateGeneralState();

    // this.gamepadControl(this._gamepadState);
  }

  private async _updateGeneralState() {
    if (Date.now() > this._lastGeneralStateUpdate + 1000) {
      this._lastGeneralStateUpdate = Date.now();

      this._connected = await this._sdk.isConnected();

      if (!this._connected) {
        this._streaming = false;
      }
    }
  }

  public async connectToDrone() {
    const droneConnected = await this._sdk.connect(this._droneIp, false);

    this._connected = droneConnected;

    console.log(this._connected);
  }

  public async takeOff() {
    if (this.connected && !this.isFlying) {
      const success = await this._sdk.takeOff();
    } else {
    }
  }

  public async land() {
    if (this.connected && this.isFlying) {
      const success = await this._sdk.land();
    } else {
    }
  }

  public async startStream() {
    if (this.connected && !this.streaming) {
      const success = await this._sdk.streamOn();
      this._streaming = success;
    }
  }

  public async stopStream() {
    if (this.connected && this.streaming) {
      const success = await this._sdk.streamOff();

      this._streaming = !success;
    }
  }

  public async speedUp() {
    if (this._speed < 100 && this.connected) {
      const succes = await this._sdk.speed(this._speed++);

      // this._lastSpeedChange = Date.now();
      console.log(this._speed);
    }
  }

  public async speedDown() {
    if (this._speed > 10 && this.connected) {
      const success = await this._sdk.speed(this._speed--);

      // this._lastSpeedChange = Date.now();
      console.log(this._speed);
    }
  }

  public async flip(direction: FlipDirections) {
    console.log("flip", direction);

    if (this.connected && this.isFlying) {
      const success = await this._sdk.flip(direction);
    }
  }

  public async updateGamepadState(gamepadDatas: number[]) {
    this._gamepadState = gamepadDatas;
  }

  public async gamepadControl(gamepadDatas: number[]) {
    const lr = gamepadDatas[2]; //Math.abs(gamepadDatas[2]) > 0.05 ? gamepadDatas[2] : 0;
    const fb = gamepadDatas[3]; //Math.abs(gamepadDatas[3]) > 0.05 ? gamepadDatas[3] : 0;
    const ud = gamepadDatas[1]; //Math.abs(gamepadDatas[1]) > 0.05 ? gamepadDatas[1] : 0;
    const yaw = gamepadDatas[0]; //Math.abs(gamepadDatas[0]) > 0.05 ? gamepadDatas[0] : 0;

    if (this.connected && this.isFlying) {
      this._sdk.rc(
        Math.round(lr * 100),
        -Math.round(fb * 100),
        -Math.round(ud * 100),
        Math.round(yaw * 100)
      );
    }
  }

  public static TELLO_IP = "192.168.10.1";
  public static TELLO_PORT = 8889;
  public static TELLO_STATE_PORT = 8890;
}
